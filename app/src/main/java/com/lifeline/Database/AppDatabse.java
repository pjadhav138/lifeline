package com.lifeline.Database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {ProductTable.class},version = 3  )
public abstract class AppDatabse extends RoomDatabase {
    public abstract ProductInterface productInterface();
}