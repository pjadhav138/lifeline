package com.lifeline.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ProductInterface {
    @Query("SELECT * FROM Products")
    List<ProductTable> getTasksList();

    @Query("SELECT * FROM Products WHERE text LIKE '%' || :query || '%' ")
    List<ProductTable> getTasksListForSearch(String query);

    @Insert
    void insert(ProductTable... tasks);
    @Insert
    void insertAll(List<ProductTable> tasks);

}
