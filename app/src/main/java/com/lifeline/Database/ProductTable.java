package com.lifeline.Database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Products")
public class ProductTable {
    @PrimaryKey(autoGenerate = true)
    private int uid;
    @ColumnInfo(name = "text")
    private String text;
    @ColumnInfo(name = "date")
    private long date;
    @ColumnInfo(name = "type")
    private String type = "";
    @ColumnInfo(name = "img")
    private String image;
    @ColumnInfo(name = "mrp")
    private String mrp;
    @ColumnInfo(name = "sell_price")
    private String sell_price;
    @ColumnInfo(name = "company")
    private String company;
    @ColumnInfo(name = "delivery_status")
    private String delivery_status;

    public ProductTable(String text, long date, String type, String image, String mrp, String sell_price, String company) {
        this.text = text;
        this.date = date;
        this.type = type;
        this.image = image;
        this.mrp = mrp;
        this.sell_price = sell_price;
        this.company = company;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(String delivery_status) {
        this.delivery_status = delivery_status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getSell_price() {
        return sell_price;
    }

    public void setSell_price(String sell_price) {
        this.sell_price = sell_price;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}

