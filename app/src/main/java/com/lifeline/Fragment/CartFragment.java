package com.lifeline.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.lifeline.Adapter.CartAdapter;
import com.lifeline.Database.ProductTable;
import com.lifeline.MainActivity;
import com.lifeline.R;
import com.lifeline.databinding.FragmentCartBinding;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CartFragment#} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends Fragment implements View.OnClickListener {
    FragmentCartBinding binding;
    private String TAG = getClass().getSimpleName();
    MainActivity activity;
    Uri imgUri;
    private Uri photouri = null;

    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        activity.setTitle("Cart");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView: ");
        // Inflate the layout for this fragment
        binding = FragmentCartBinding.inflate(inflater, container, false);
        binding.rlCart.setOnClickListener(this);
        binding.placeOrder.setOnClickListener(this);
        binding.attachPriscription.setOnClickListener(this);
        File file = new File(Environment.getExternalStorageDirectory() + "/images/");
        if (file.exists()) {
            imgUri = Uri.parse(new File(Environment.getExternalStorageDirectory() + "/images/prescription.jpg").getAbsolutePath());
        } else {
            file.mkdir();
            imgUri = Uri.parse(new File(Environment.getExternalStorageDirectory() + "/images/prescription.jpg").getAbsolutePath());
        }
        binding.message.setVisibility(activity.cart.size() == 0 ? View.VISIBLE : View.GONE);
        binding.cartList.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.cartList.setAdapter(new CartAdapter(getContext(), activity.cart));
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.attach_priscription:
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
                            }, MainActivity.PERMISSION_WRITE_STORAGE);
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
                    startActivityForResult(intent, MainActivity.TAKE_PICTURE);
                }
                break;
            case R.id.rl_cart:
                break;
            case R.id.place_order:

                sendwhatsappMessage2();
//                sendwhatsappMessage1();
              /*  try {
                    Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                    whatsappIntent.setType("text/plain");
                    whatsappIntent.setPackage("com.whatsapp");
                    String text = "";
                    for (ProductTable table : activity.cart) {
                        text += "\n" + table.getText() + "-" + table.getCompany();
                    }
                    String url = "https://api.whatsapp.com/send?phone=918898527975&text=" + URLEncoder.encode(text, "UTF-8");
//                    String url = "https://wa.me/918898527975?text=" + URLEncoder.encode(text, "UTF-8");
//                    whatsappIntent.putExtra(Intent.EXTRA_TEXT, text);
//                    whatsappIntent.setData(Uri.parse(url));
                    whatsappIntent.setData(Uri.parse(url));
//                    whatsappIntent.putExtra(Intent.EXTRA_STREAM, imgUri);
                    whatsappIntent.setType("image/*");
                    whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    activity.startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(activity, "Whatsapp have not been installed.", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }*/
                break;
        }

    }

    private void sendwhatsappMessage1() {
        String text = "Look at my awesome picture";
        Uri pictureUri = Uri.parse("file://my_picture");
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imgUri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share images..."));
    }

    private void sendwhatsappMessage() {
        try {
            Uri uri = Uri.parse("android.resource://com.lifeline/drawable/no_image_available");

            String text = "";
            for (ProductTable table : activity.cart) {
                text += "\n" + table.getText() + "-" + table.getCompany();
            }

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);

            String url = "https://api.whatsapp.com/send?phone=" + "918898527975" + "&text=" + URLEncoder.encode(text, "UTF-8");
            intent.setPackage("com.whatsapp");
            intent.setData(Uri.parse(url));
          /*intent.putExtra(Intent.EXTRA_TEXT, text);
          intent.setType("text/plain");
          intent.putExtra(Intent.EXTRA_STREAM,uri);
          intent.setType("image/jpeg");
          intent.setPackage("com.whatsapp");*/
            intent.putExtra(Intent.EXTRA_STREAM, uri);

            startActivity(intent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void sendwhatsappMessage2() {
        try {
            Uri uri = Uri.parse("android.resource://com.lifeline/drawable/no_image_available");

            String text = "";
            for (ProductTable table : activity.cart) {
                text += "\n" + table.getText() + "-" + table.getCompany();
            }

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);

            String url = "https://api.whatsapp.com/send?phone=" + "918898527975" + "&text=" + URLEncoder.encode(text, "UTF-8");
            shareIntent.setPackage("com.whatsapp");
//            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_TEXT, text);
//            shareIntent.setData(Uri.parse(url));

//            Uri uri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/drawable/ford_focus_2014");
            try {
                InputStream stream = getActivity().getContentResolver().openInputStream(photouri);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            shareIntent.putExtra(Intent.EXTRA_STREAM, photouri);

            shareIntent.putExtra("jid", "918898527975" + "@s.whatsapp.net");
            startActivity(shareIntent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult: requestCode - " + requestCode);
        Log.e(TAG, "onActivityResult: resultCode - " + resultCode + data.getExtras());

        switch (resultCode) {
            case Activity.RESULT_OK:
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                File file = new File(getActivity().getDataDir() + "/images/");
                if (!file.exists())
                    file.mkdir();
                try (FileOutputStream out = new FileOutputStream(getActivity().getDataDir() + "/images/prescription.jpg")) {
                    bitmap.compress(Bitmap.CompressFormat.WEBP, 100, out); // bmp is your Bitmap instance
                    String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), bitmap, "Title", null);
                    photouri = Uri.parse(path);
                    binding.attachPriscription.setVisibility(View.GONE);
                    binding.placeOrder.setVisibility(View.VISIBLE);
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}