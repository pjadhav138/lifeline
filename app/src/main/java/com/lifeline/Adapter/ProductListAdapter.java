package com.lifeline.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.lifeline.Database.ProductTable;
import com.lifeline.MainActivity;
import com.lifeline.R;
import com.lifeline.databinding.ItemRowBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> implements Filterable {
    private final String TAG = getClass().getSimpleName();
    Context context;
    List<ProductTable> datanotFiltered;
    List<ProductTable> postDataItemList;
//    ItemRowBinding binding;

    public ProductListAdapter(Context context, List<ProductTable> postDataItemList) {
        setHasStableIds(true);
        this.context = context;
        this.postDataItemList = postDataItemList;
        this.datanotFiltered = postDataItemList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final ProductTable item = postDataItemList.get(position);
        Log.e(TAG, "onBindViewHolder: "+item.getText() );
        holder.textView.setText(item.getText());
        if (((MainActivity)context).cart.contains(item))
        holder.cart.setText("Remove");else holder.cart.setText("Add to Card");
        holder.cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView)v;
                if (textView.getText().toString().equals("Add to Card")) {
                    textView.setText("Remove");
                    setCount(true,item);
                } else {
                    textView.setText("Add to Card");
                    setCount(false,item);
                }
            }
        });
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        Glide.with(context).load(postDataItemList.get(position).getImage())
        .override(200, 200) .placeholder(R.drawable.ic_launcher_background).into(holder.imageView);
       /* Picasso.get()
                .load(postDataItemList.get(position).getImage())
                .resize(1500, 0)
                .placeholder(R.drawable.ic_launcher_background)
//                .error(R.drawable.download_error)
                .into(binding.imgRoomType);*/
    }

    @Override
    public int getItemCount() {
        return postDataItemList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Log.e(TAG, "performFiltering: "+charSequence );
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    postDataItemList = datanotFiltered;
                } else {
                    List<ProductTable> filteredList = new ArrayList<>();
                    for (ProductTable row : datanotFiltered) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getText().toLowerCase().contains(charString.toLowerCase()) || row.getCompany().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    Log.e(TAG, "performFiltering: "+filteredList.size() );

                    postDataItemList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = postDataItemList;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                postDataItemList = (ArrayList<ProductTable>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

  /*  public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ItemRowBinding.bind(itemView);
        }
    }*/


    private void setCount(boolean add, ProductTable productTable) {
        if (add) {
            ((MainActivity) context).cart.add(productTable);
            ((MainActivity) context).setMenuCounter(((MainActivity) context).cart.size());
        } else {
            ((MainActivity) context).cart.remove(productTable);
            ((MainActivity) context).setMenuCounter(((MainActivity) context).cart.size());
        }
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    public void setPostDataItemList(List<ProductTable> list){
        this.postDataItemList= list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;
        Button cart;
        CardView card_view;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tvTypeName);
            imageView = itemView.findViewById(R.id.img_roomType);
            cart = itemView.findViewById(R.id.add_cart);
            card_view = itemView.findViewById(R.id.card_view);

        }
    }
}