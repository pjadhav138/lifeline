package com.lifeline;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.lifeline.Adapter.ProductListAdapter;
import com.lifeline.Database.AppDatabse;
import com.lifeline.Database.ProductTable;
import com.lifeline.Fragment.CartFragment;
import com.lifeline.Tools.CSVReader;
import com.lifeline.Tools.CSVWriter;
import com.lifeline.databinding.ActivityMainBinding;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final int PERMISSION_WRITE_STORAGE = 100;
    public static final int TAKE_PICTURE = 101;
    ActivityMainBinding binding;
    AppDatabse db;
    private String TAG = getClass().getSimpleName();
    public List<ProductTable> cart;
    ProductListAdapter adapter;
    private SearchView searchView;
    GoogleApiClient googleApiClient;
    private int PHONE_NUMBER_RC = 201;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(Auth.CREDENTIALS_API)
                .build();
//        requestPhoneNumber();
//        showHint();
        cart = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
                    }, PERMISSION_WRITE_STORAGE);
        }

        binding.cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame, new CartFragment()).commit();
            }
        });
        binding.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.search.setIconified(false);
            }
        });
        binding.search.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.getFilter().filter("");
                return false;
            }
        });
        binding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                binding.search.setIconified(true);
                Log.e(TAG, "onQueryTextSubmit: " + query);
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.e(TAG, "onQueryTextChange: " + newText);
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        populateData();
//        ExportCSV();


    }

    public void requestPhoneNumber() {
//        phoneNumberCallback = callback;
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(), PHONE_NUMBER_RC, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "Could not start hint picker Intent", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHONE_NUMBER_RC) {
            if (resultCode == RESULT_OK) {
                Credential cred = data.getParcelableExtra(Credential.EXTRA_KEY);

                Log.e(TAG, "onActivityResult: " + cred.getId());
            }
        }
    }

    private void populateData() {
        db = Room.databaseBuilder(MainActivity.this, AppDatabse.class, "ProductsData")
                /*.addMigrations(MIGRATION_1_2)*/.allowMainThreadQueries().build();

        List<ProductTable> list = db.productInterface().getTasksList();
        Log.e(TAG, "onCreate: " + list.size());
        if (list.size() < 1) {
            ImportInDatabase();
        }
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        binding.productist.setLayoutManager(mLayoutManager);
        adapter = new ProductListAdapter(this, list);
        binding.productist.setAdapter(adapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_WRITE_STORAGE:
                if (grantResults[0] != 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this)
                            .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                                                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
                                        }, PERMISSION_WRITE_STORAGE);
                                    }
                                }
                            })
                            .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setMessage("Permission require to Populate Data.");
                    builder.create();
                    builder.show();
                } else {
                    populateData();
                }
                break;
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        Log.e(TAG, "onContextItemSelected: ");
        return super.onContextItemSelected(item);
    }

    private void ExportCSV() {
        File exportDir = new File(Environment.getExternalStorageDirectory(), "");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }
        Log.e(TAG, "ExportCSV: " + exportDir);
        File file = new File(exportDir, "Product.csv");
        try {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            Cursor curCSV = db.query("SELECT * FROM Products", null);
            csvWrite.writeNext(curCSV.getColumnNames());
            while (curCSV.moveToNext()) {
                //Which column you want to exprort
                String arrStr[] = new String[curCSV.getColumnCount()];
                for (int i = 0; i < curCSV.getColumnCount() - 1; i++)
                    arrStr[i] = curCSV.getString(i);
                csvWrite.writeNext(arrStr);
            }
            csvWrite.close();
            curCSV.close();
            Toast.makeText(this, "Exported", Toast.LENGTH_LONG).show();
        } catch (Exception sqlEx) {
            Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
        }
    }

    private void ImportCSV() throws IOException {
        CSVReader csvReader = new CSVReader(new FileReader(Environment.getExternalStorageDirectory() + "/" + "Product.csv"));
        String[] nextLine;
        int count = 0;
        StringBuilder columns = new StringBuilder();
        StringBuilder value = new StringBuilder();

        while ((nextLine = csvReader.readNext()) != null) {
            // nextLine[] is an array of values from the line
            for (int i = 0; i < nextLine.length - 1; i++) {
                if (count == 0) {
                    if (i == nextLine.length - 2)
                        columns.append(nextLine[i]);
                    else
                        columns.append(nextLine[i]).append(",");
                } else {
                    if (i == nextLine.length - 2)
                        value.append("'").append(nextLine[i]).append("'");
                    else
                        value.append("'").append(nextLine[i]).append("',");
                }
            }

        }
        Log.e(TAG, columns + "-------" + value);

    }

    private void ImportData() throws IOException {
        FileReader file = new FileReader(Environment.getExternalStorageDirectory() + "/" + "Product.csv");
        BufferedReader buffer = new BufferedReader(file);
        String line = "";
        String tableName = "TABLE_NAME";
        String columns = "text, name, dt1, dt2, dt3";
        String str1 = "INSERT INTO " + tableName + " (" + columns + ") values(";
        String str2 = ");";

        List<ProductTable> data = new ArrayList<>();
//        db.beginTransaction();
        while ((line = buffer.readLine()) != null) {
            StringBuilder sb = new StringBuilder(str1);
            String[] str = line.split(",");
            sb.append("'" + str[0] + "',");
            sb.append(str[1] + "',");
            sb.append(str[2] + "',");
            sb.append(str[3] + "'");
            sb.append(str[4] + "'");
            sb.append(str2);
//            data.add(new ProductTable(str[0],Calendar.getInstance().getTimeInMillis(),str[1],str[2],str[3],str[4],str[5]));
            Log.e(TAG, "ImportData: " + sb);
        }
        db.productInterface().insertAll(data);

//        db.setTransactionSuccessful();
//        db.endTransaction();
    }


    public void setMenuCounter(int count) {
        binding.cartBadge.setText(count > 0 ? String.valueOf(count) : "");
        binding.cartBadge.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
    }

    private void copyDataBase() throws IOException {

        //Open your local db as the input stream
        InputStream myInput = getApplicationContext().getAssets().open("ProductsData");

        // Path to the just created empty db
        String outFileName = Environment.getDataDirectory() + "/data/" + getPackageName() + "/databases/" + "ProductsData";
        if (new File(outFileName).exists()) return;
        String file = Environment.getDataDirectory() + "/" + getPackageName() + "/databases/";
        if (!new File(file).exists()) {
            new File(file).mkdir();
        }
        Log.e(TAG, "copyDataBase: not exist." + outFileName);
        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    private void ImportInDatabase() {
        try {
            InputStream myInput = getApplicationContext().getAssets().open("Product.csv");
            InputStreamReader isr = new InputStreamReader(myInput);
            BufferedReader buffer = new BufferedReader(isr);
            String line;
            List<ProductTable> list = new ArrayList<>();
            while ((line = buffer.readLine()) != null) {
                String[] str = line.split(",");
                list.add(new ProductTable(str[0], Calendar.getInstance().getTimeInMillis(), str[1], str[2], str[3], str[4], str[5]));
            }
            db.productInterface().insertAll(list);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchmenu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        final MenuItem item = menu.findItem(R.id.action_search);
       searchView.setOnCloseListener(new SearchView.OnCloseListener() {
           @Override
           public boolean onClose() {
               Log.e(TAG, "onClose: " );
               binding.title.setVisibility(View.VISIBLE);
               return false;
           }
       });

       searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
           @Override
           public void onFocusChange(View v, boolean hasFocus) {
               Log.e(TAG, "onFocusChange: " );
               if (hasFocus){
                   binding.title.setVisibility(View.GONE);
               }
           }
       });

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e(TAG, "onQueryTextSubmit: " );
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                Log.e(TAG, "onQueryTextChange: " );
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }
*/

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
        if (fragment instanceof CartFragment) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        } else {
            finish();
        }

    }

    public void setTitle(String title) {
        binding.title.setText(title);
    }
    private void showHint() {
//        ui.clearKeyboard();
        HintRequest hintRequest = new HintRequest.Builder()
                .setHintPickerConfig(new CredentialPickerConfig.Builder()
                        .setShowCancelButton(true)
                        .build())
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent =
                Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(),PHONE_NUMBER_RC , null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "Could not start hint picker Intent", e);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (bundle!=null && !bundle.isEmpty()){
            Log.e(TAG, "onConnected: "+bundle.toString() );
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: " + connectionResult.getErrorMessage());
    }
}